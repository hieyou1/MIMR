# MIMR
It mimifies things.<br /><br />
What that means is you can use direct links to scripts without worrying about if the browser THINKS it's a JS file.<br />
Here's an example.<br />
If you click on a JS script and click "Raw" in any GitHub repo (without going through a CDN) and you put that link in your website, it should work, right?<br />
Here's one of my files on one of my repos to test it out.<br />
[https://github.com/hieyou1/disableclicks/blob/master/disableclicks.js](https://github.com/hieyou1/disableclicks/blob/master/disableclicks.js)<br />
If you click "Raw" on that site, it gives you a script. Great.<br />
BUT!!!!!!!!!!!<br />
The browser thinks that script is a plain text file.<br />
If you insert that script into your code, you literally CAN'T use the code you imported. <br />
And that's where this script comes in.<br />
Import this script, run it, and it generates a fully compatible script that you can use!<br />
It's that simple! Read the instructions below to find out how to use it.<br /><br />
If you want to mimify something, it's simple. Anything in square brackets [these] isn't required.<br />
Make SURE you are passing a URL that is preceded by http:// or https:// and that the URL is wrapped in quotes!<br />
`mimify(file URL {unmimed}, [resulting ID, callback]);`<br />
If you implement a callback, you must also pass a resulting ID.<br />
If you want to know that it's done, but you don't want to write a whole function for it, instead of the callback type console.log. Your element will be logged to the console when the script is completed.<br />
Also if you implement a callback, you must pass it without any parentheses (aka DON'T USE THESE when passing your callback >) and without quotes around it "so don't use these>". Make sure to put quotes arount the file URL, though!<br />
Also also if you implement a callback, it will be passed the resulting script element as the first and only argument. Use it if you want to, but you don't have to.<br />
Use the callback feature as kind of like a document.ready for your mimified script.<br />
Make sure that you don't use the same callback for two mimified scripts! You don't want the script executing twice.<br />
(Or you do, but you know what I mean.)<br />
Alright, thanks for reading, and have fun with your now fully functional scripts!<br />
# Curious about how it works?
Go into the mimr.js file (no minification). It's all commented.<br />
(TL;DR It encodes the JS file you pass it with Base64, and injects some headers. Then it adds that script to the site.)