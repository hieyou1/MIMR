function mimify(fileloc, resid, readyCall) { // fileloc = File Location (On the Web), resid = Result ID (optional), readyCall = On Fully Mimified Callback (optional, if this argument is passed we will pass the script argument to the callback [kinda like a document.ready, except {your mimified script here}.ready, if that makes any sense])
  function httpGet(t,e){var n=new XMLHttpRequest;n.onreadystatechange=function(){4==n.readyState&&200==n.status&&e(n.responseText)},n.open("GET",t,!0),n.send(null)}
  function getComplete(response) {
    console.debug("MIMR DEBUG: Got a response from the GET request. Response is: " + response);
    var res64 = btoa(response); //res64 = Response (base64 Encoded)
    var res64wnh = "data:application/javascript;base64," + res64; // res64wnh = Response (base64 Encoded) with necessary headers (MIME headers)
    console.debug("MIMR DEBUG: Finished adding headers/doing base64 encoding. Fully formatted response is: " + res64wnh);
    var scrElem  =  document.createElement("script"); // scrElem = Script Element
    if (resid) {
      scrElem.setAttribute("id", resid);
    }
    scrElem.setAttribute("src", res64wnh);
    scrElem.setAttribute("type", "text/javascript");
    console.debug("MIMR DEBUG: Finished adding all attributes to element. Final element will be logged shortly.");
    document.body.appendChild(scrElem);
    console.debug("MIMR DEBUG: Element created and the script is ready for use. The element is logged below.");
    console.debug(scrElem);
    if (readyCall) {
      console.debug("MIMR DEBUG: MIMR scripts finished. Triggering callback logged below.");
      console.debug(readyCall);
      readyCall(scrElem);
    } else {
      console.debug("MIMR DEBUG: MIMR scripts finished. No callback was specified. Exiting");
    }
  }
  httpGet(fileloc, getComplete);
  console.debug("MIMR DEBUG: GET Request has been made for " + fileloc + ". If you don't see anything else right now, the GET request is still in progress.");
}
